use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :f1sty, F1styWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :f1sty, F1sty.Repo,
  username: "postgres",
  password: "postgres",
  database: "f1sty_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
