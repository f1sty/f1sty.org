defmodule F1sty.Api.Weather do
  @moduledoc """
  Documentation for Wwo.
  """

  @wwo_key Application.get_env(:f1sty, :wwo_key)
  @base_url "http://api.worldweatheronline.com"

  def get_weather(location) do
    %{"current_condition" => [condition], "request" => [%{"query" => loc}]} =
      HTTPoison.get!(@base_url <> "/premium/v1/weather.ashx", [],
        params: [key: @wwo_key, q: location, format: "json", num_of_days: 1]
      )
      |> Map.get(:body)
      |> Jason.decode!()
      |> Map.get("data")

    condition
    |> Enum.map(fn {k, v} ->
      case is_list(v) do
        true ->
          %{"value" => v} = List.first(v)
          {k, v}

        _ ->
          {k, v}
      end
    end)
    |> Enum.into(%{})
    |> Map.put("location", loc)
  end
end
