defmodule F1sty.Repo do
  use Ecto.Repo,
    otp_app: :f1sty,
    adapter: Ecto.Adapters.Postgres
end
