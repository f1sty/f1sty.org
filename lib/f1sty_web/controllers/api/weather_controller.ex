defmodule F1styWeb.Api.WeatherController do
  use F1styWeb, :controller

  def show(conn, %{"location" => location}) do
    weather = F1sty.Api.Weather.get_weather(location)
    render(conn, "show.json", %{"data" => weather})
  end
end
