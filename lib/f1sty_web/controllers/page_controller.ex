defmodule F1styWeb.PageController do
  use F1styWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
