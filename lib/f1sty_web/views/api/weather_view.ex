defmodule F1styWeb.Api.WeatherView do
  use F1styWeb, :view

  def render("show.json", params) do
    params
    |> Map.get("data")
  end
end
