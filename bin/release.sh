#!/bin/sh

VERSION=$(grep version mix.exs | cut -d: -f 2 | sed "s/,//" | sed "s/ //" | tr -d '"')
mix deps.get --only prod
cd assets 
npm install && node node_modules/webpack/bin/webpack.js --mode production && cd ..
MIX_ENV=prod mix do compile, phx.digest, ecto.setup, release --env=prod --upgrade
